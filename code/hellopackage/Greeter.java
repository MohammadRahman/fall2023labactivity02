package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;

public class Greeter{

    public static void main (String[] args){

        Scanner reader = new Scanner(System.in);
        Random rand = new Random();

        System.out.println("Enter a number:");
        int number = reader.nextInt();

        int newNumber = Utilities.doubleMe(number);
        System.out.println("The double: " + newNumber);

    }

}